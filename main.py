import sys
import sqlite3

from PyQt5 import uic, QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidgetItem, \
    QDialog

MAIN_WINDOW_UIC_PATH = 'UI/main.ui'
ADD_EDIT_FORM_UIC_PATH = 'UI/addEditCoffeeForm.ui'
COFFEE_DB_PATH = 'data/coffee.sqlite'
WRONG_NUMBER_OF_SELECTED_ROWS_MESSAGE = 'Invalid number of selected rows'
EDIT_FORM_TITLE = 'Edit coffee'
ADD_FORM_TITLE = 'Add coffee'


class Ui_addEditCoffeeForm(object):
    def setupUi(self, addEditCoffeeForm):
        addEditCoffeeForm.setObjectName("addEditCoffeeForm")
        addEditCoffeeForm.resize(364, 298)
        self.gridLayout = QtWidgets.QGridLayout(addEditCoffeeForm)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(addEditCoffeeForm)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.label_4 = QtWidgets.QLabel(addEditCoffeeForm)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(addEditCoffeeForm)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(addEditCoffeeForm)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.label_5 = QtWidgets.QLabel(addEditCoffeeForm)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 4, 0, 1, 1)
        self.label_6 = QtWidgets.QLabel(addEditCoffeeForm)
        self.label_6.setObjectName("label_6")
        self.gridLayout.addWidget(self.label_6, 5, 0, 1, 1)
        self.description = QtWidgets.QPlainTextEdit(addEditCoffeeForm)
        self.description.setObjectName("description")
        self.gridLayout.addWidget(self.description, 3, 1, 1, 1)
        self.fragmentation = QtWidgets.QLineEdit(addEditCoffeeForm)
        self.fragmentation.setObjectName("fragmentation")
        self.gridLayout.addWidget(self.fragmentation, 2, 1, 1, 1)
        self.roastingDegree = QtWidgets.QComboBox(addEditCoffeeForm)
        self.roastingDegree.setObjectName("roastingDegree")
        self.gridLayout.addWidget(self.roastingDegree, 1, 1, 1, 1)
        self.title = QtWidgets.QLineEdit(addEditCoffeeForm)
        self.title.setObjectName("title")
        self.gridLayout.addWidget(self.title, 0, 1, 1, 1)
        self.cost = QtWidgets.QSpinBox(addEditCoffeeForm)
        self.cost.setMaximum(99999)
        self.cost.setObjectName("cost")
        self.gridLayout.addWidget(self.cost, 4, 1, 1, 1)
        self.volume = QtWidgets.QSpinBox(addEditCoffeeForm)
        self.volume.setMaximum(99999)
        self.volume.setObjectName("volume")
        self.gridLayout.addWidget(self.volume, 5, 1, 1, 1)
        self.saveChangesBtn = QtWidgets.QPushButton(addEditCoffeeForm)
        self.saveChangesBtn.setObjectName("saveChangesBtn")
        self.gridLayout.addWidget(self.saveChangesBtn, 6, 0, 1, 2)

        self.retranslateUi(addEditCoffeeForm)
        QtCore.QMetaObject.connectSlotsByName(addEditCoffeeForm)

    def retranslateUi(self, addEditCoffeeForm):
        _translate = QtCore.QCoreApplication.translate
        addEditCoffeeForm.setWindowTitle(_translate("addEditCoffeeForm",
                                                    "addEditCoffeeForm"))
        self.label.setText(_translate("addEditCoffeeForm", "Title"))
        self.label_4.setText(_translate("addEditCoffeeForm", "Description"))
        self.label_3.setText(_translate("addEditCoffeeForm", "Fragmentation"))
        self.label_2.setText(_translate("addEditCoffeeForm",
                                        "Degree of roasting"))
        self.label_5.setText(_translate("addEditCoffeeForm", "Cost"))
        self.label_6.setText(_translate("addEditCoffeeForm", "Volume"))
        self.saveChangesBtn.setText(_translate("addEditCoffeeForm", "Save"))


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(580, 430)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.addCoffeeBtn = QtWidgets.QPushButton(self.centralwidget)
        self.addCoffeeBtn.setObjectName("addCoffeeBtn")
        self.gridLayout.addWidget(self.addCoffeeBtn, 1, 1, 1, 1)
        self.changeCoffeeBtn = QtWidgets.QPushButton(self.centralwidget)
        self.changeCoffeeBtn.setObjectName("changeCoffeeBtn")
        self.gridLayout.addWidget(self.changeCoffeeBtn, 1, 0, 1, 1)
        self.coffeeTable = QtWidgets.QTableWidget(self.centralwidget)
        self.coffeeTable.setObjectName("coffeeTable")
        self.coffeeTable.setColumnCount(0)
        self.coffeeTable.setRowCount(0)
        self.gridLayout.addWidget(self.coffeeTable, 0, 0, 1, 2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Эспрессо"))
        self.addCoffeeBtn.setText(_translate("MainWindow", "Add"))
        self.changeCoffeeBtn.setText(_translate("MainWindow", "Change"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.initUi()

    def initUi(self):
        self.connection = sqlite3.connect(COFFEE_DB_PATH)
        self.changeCoffeeBtn.clicked.connect(self.change_coffee)
        self.addCoffeeBtn.clicked.connect(self.add_coffee)
        self.load_coffee_info()

    def load_coffee_info(self):
        cursor = self.connection.cursor()

        headers = cursor.execute('''
        PRAGMA table_info(coffee)
        ''').fetchall()
        result = cursor.execute('''
        SELECT * FROM coffee
        ''').fetchall()

        self.coffeeTable.setColumnCount(len(headers))
        self.coffeeTable.setHorizontalHeaderLabels(
            map(lambda row: row[1], headers))
        self.coffeeTable.setRowCount(0)

        for i, row in enumerate(result):
            self.coffeeTable.setRowCount(
                self.coffeeTable.rowCount() + 1)
            for j, elem in enumerate(row):
                self.coffeeTable.setItem(
                    i, j, QTableWidgetItem(str(elem)))
        self.coffeeTable.resizeColumnsToContents()

    def change_coffee(self):
        selected_ranges = self.coffeeTable.selectedRanges()
        if not selected_ranges or \
                selected_ranges and selected_ranges[0].rowCount() != 1:
            self.statusbar.showMessage(WRONG_NUMBER_OF_SELECTED_ROWS_MESSAGE)
        else:
            self.statusbar.showMessage('')
            current_row = selected_ranges[0].topRow()
            row_id = self.coffeeTable.item(current_row, 0).text()

            self.addEditCoffeeForm = AddEditCoffeeForm(
                'edit', self, self.connection, row_id)
            self.addEditCoffeeForm.show()

    def add_coffee(self):
        self.addEditCoffeeForm = AddEditCoffeeForm(
            'add', self, self.connection)
        self.addEditCoffeeForm.show()


class AddEditCoffeeForm(QDialog, Ui_addEditCoffeeForm):
    def __init__(self, type='add', *args):
        super().__init__()
        self.setupUi(self)
        self.initUi(type, *args)

    def initUi(self, type, *args):
        self.type = type
        if type == 'add':
            self.parent, self.connection = args
            self.setWindowTitle(ADD_FORM_TITLE)
            self.load_roasting_degree()
        elif type == 'edit':
            self.parent, self.connection, self.row_id = args
            self.setWindowTitle(EDIT_FORM_TITLE)
            self.load_roasting_degree()
            self.load_coffee_parameters()

        self.saveChangesBtn.clicked.connect(self.save_changes)

    def save_changes(self):
        cursor = self.connection.cursor()

        title = self.title.text()
        roastingDegree = cursor.execute(f'''
        SELECT id FROM roastingDegree
        WHERE name = '{self.roastingDegree.currentText()}'
        ''').fetchone()[0]
        fragmentation = self.fragmentation.text()
        description = self.description.toPlainText()
        cost = self.cost.value()
        volume = self.volume.value()

        if self.type == 'add':
            cursor.execute(f'''
            INSERT INTO coffee(title, roastingDegree, fragmentation, 
            description, cost, volume)
            VALUES ('{title}', {roastingDegree}, '{fragmentation}', 
            '{description}', {cost}, {volume})
            ''')
        elif self.type == 'edit':
            cursor.execute(f'''
            UPDATE coffee
            SET title = '{title}', roastingDegree = {roastingDegree}, 
            fragmentation = '{fragmentation}', description = '{description}', 
            cost = {cost}, volume = {volume}
            WHERE id = {self.row_id}
            ''')

        self.connection.commit()
        self.parent.load_coffee_info()
        self.close()

    def load_coffee_parameters(self):
        cursor = self.connection.cursor()
        parameters = cursor.execute(f'''
        SELECT * FROM coffee
        WHERE id = {int(self.row_id)}
        ''').fetchone()

        title, roastingDegree, fragmentation, description, cost, volume = \
            parameters[1:]

        self.title.setText(title)
        self.roastingDegree.setCurrentIndex(roastingDegree - 1)
        self.fragmentation.setText(fragmentation)
        self.description.setPlainText(description)
        self.cost.setValue(cost)
        self.volume.setValue(volume)

    def load_roasting_degree(self):
        cursor = self.connection.cursor()
        result = cursor.execute('''
        SELECT name FROM roastingDegree
        ''').fetchall()

        for item in result:
            self.roastingDegree.addItem(item[0])


def except_hook(cls, exception, traceback):
    sys.__excepthook__(cls, exception, traceback)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.excepthook = except_hook
    sys.exit(app.exec())
